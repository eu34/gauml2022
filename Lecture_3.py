# a = 12
# b = 13
# print(a, b)
# a, b = b, a
# c = a # 8
# a = b # 9
# b = c # 8
# a = a + b # 17
# b = a - b # 8
# a = a - b # 9
# print(a, b)
# print(bin(a))
# print(bin(b))
# a = a ^ b  #0001
# b = a ^ b  #1100
# a = a ^ b  #1101
# print(a, b)
 # print(bin(a))
# print(bin(a>>1))
# print(bin(b))
# print(bin(b>>1))

# m = 6
# n = 7
# print(bin(m), bin(n))
# print(bin(m^n))

# mylist = [3, 4, 5, 98, 232, 4, 33, 4]
# print(mylist[3])
# print(mylist[3:5])
# print(mylist[3:])
# print(mylist[:3])
# mylist[3] = 0
# print(mylist)

mytuple = (3, 4, 5, 98, 232, 4, 33, 4)

# print(mytuple)
# print(mytuple[2:5])
# mytuple[2] = 7

# myset = {3, 0, 4, 5, 98, 232, 4, 33, 2, 4}
# print(myset)

def f1():
    print("One")

f1()

def f2(name="Dato"):
    print(name)

f2("Achi")
f2()

